package com.jamkXml.musicShowFinder.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jamkXml.musicShowFinder.beans.ShowsBean;

public class Show {
	
	private String artist;
	private String showId;
	private String title;
	private String tour;
	private Date showDate;
	private Date setTime;
	private Date doorTime;
	private String venueName;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String countryName;
	private long latitude;
	private long longitude;
	private String comments;
	private String buyOnline;
	private String timezone;
	
	private SimpleDateFormat sdfDate,sdfTime;
	
	ShowsBean  sb;
	
	
	public boolean isLatLong() {
		return latitude != -1 && longitude != -1;
	}
	
	
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getShowId() {
		return showId;
	}
	public void setShowId(String showId) {
		this.showId = showId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTour() {
		return tour;
	}
	public void setTour(String tour) {
		this.tour = tour;
	}
	public String getShowDate() {
		return showDate == null ? "":sdfDate.format(showDate);
	}
	public void setShowDate(Date showDate) {
		this.showDate = showDate;
	}
	public String getSetTime() {
		return setTime == null ? "":sdfTime.format(setTime);
	}
	public void setSetTime(Date setTime) {
		this.setTime = setTime;
	}
	public String getDoorTime() {
		return doorTime == null ? "":sdfTime.format(doorTime);
	}
	
	public String getLatLong(){
		return latitude+", "+longitude;
	}
	
	public Show(String artist, String showId, String title, String tour,
			Date showDate, Date setTime, Date doorTime, String venueName,
			String address, String city, String state, String zip,
			String country, String countryName, long latitude, long longitude,
			String comments, String buyOnline, String timezone, ShowsBean sb) {
		this.artist = artist;
		this.showId = showId;
		this.title = title;
		this.tour = tour;
		this.showDate = showDate;
		this.setTime = setTime;
		this.doorTime = doorTime;
		this.venueName = venueName;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.country = country;
		this.countryName = countryName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.comments = comments;
		this.buyOnline = buyOnline;
		this.timezone = timezone;
		this.sb = sb;
		this.sdfDate = new SimpleDateFormat("dd/MM/yyyy");
		this.sdfTime = new SimpleDateFormat("HH:mm");
	}

	public Show(ShowsBean sb) {
		this.artist = "";
		this.showId = "";
		this.title = "";
		this.tour = "";
		this.showDate = null;
		this.setTime = null;
		this.doorTime = null;
		this.venueName = "";
		this.address = "";
		this.city = "";
		this.state = "";
		this.zip = "";
		this.country = "";
		this.countryName = "";
		this.latitude = -1;
		this.longitude = -1;
		this.comments = "";
		this.buyOnline = "";
		this.timezone = "";
		this.sb = sb;
	}
	public void setDoorTime(Date doorTime) {
		this.doorTime = doorTime;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public long getLatitude() {
		return latitude;
	}
	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}
	public long getLongitude() {
		return longitude;
	}
	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getBuyOnline() {
		return buyOnline;
	}
	public void setBuyOnline(String buyOnline) {
		this.buyOnline = buyOnline;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
}
