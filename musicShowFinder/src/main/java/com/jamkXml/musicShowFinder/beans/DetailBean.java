package com.jamkXml.musicShowFinder.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.jamkXml.musicShowFinder.data.Show;

@ManagedBean(name = "detailBean")
@SessionScoped
public class DetailBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3950247681189168761L;
	private MapModel model = new DefaultMapModel();
	private Marker marker;
	private Show show;
	private String mapCenter;
	
	public void onMarkerSelect(OverlaySelectEvent event){
		System.out.println("ON MARKER SELECT "+((Marker)event.getOverlay()).getTitle());
		this.marker = (Marker) event.getOverlay();
	}
	
	public void onGeocode(GeocodeEvent event){
		model.getMarkers().clear();
		if(show.getLatitude() != -1 && show.getLongitude() != -1){
			mapCenter = show.getLatitude() + "," + show.getLongitude();
			Marker m = new Marker(new LatLng(show.getLatitude(), show.getLongitude()),show.getArtist());
			m.setClickable(true);
			model.addOverlay(m);
		}else{
			List<GeocodeResult> results = event.getResults();
			if (results != null && !results.isEmpty()) {
	        	GeocodeResult result = results.get(0);
	            LatLng center = result.getLatLng();
	            mapCenter = center.getLat() + "," + center.getLng();
	            Marker m = new Marker(result.getLatLng(), result.getAddress());
				m.setClickable(true);
				model.addOverlay(m);
	        }
		}
		
	}
	
	public void setShow(Show show) {
		this.show = show;
	}
	
	
	public String getMapCenter() {
		return mapCenter;
	}

	public void setMapCenter(String mapCenter) {
		this.mapCenter = mapCenter;
	}

	public Show getShow() {
		return show;
	}

	
	
	public MapModel getModel() {
		return model;
	}

	public void setModel(MapModel model) {
		this.model = model;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	
}
