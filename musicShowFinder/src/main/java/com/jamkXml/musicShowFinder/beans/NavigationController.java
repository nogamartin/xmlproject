package com.jamkXml.musicShowFinder.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.jamkXml.musicShowFinder.data.Show;

@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController {
	
	public String showDetailPage(Show show){
		FacesContext context = FacesContext.getCurrentInstance();
		DetailBean bean = context.getApplication().evaluateExpressionGet(context, "#{detailBean}", DetailBean.class);
		bean.setShow(show);
		System.out.println("SHOWDETAILPAGE");
	    return "detailpage";
	}
}
