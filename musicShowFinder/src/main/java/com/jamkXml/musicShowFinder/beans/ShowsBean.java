package com.jamkXml.musicShowFinder.beans;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.jamkXml.musicShowFinder.data.Show;

@ManagedBean(name = "shows")
@SessionScoped
public class ShowsBean {

    private static final String API_ENDPOINT = "https://tours-api.dsp.wmg.com/shows.xml?";

    private LinkedList<Show> shows;
    private Show filterShow;
    private Show selectedShow;

    public ShowsBean() {
        System.out.println("ShowBean instantiated");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        timeFormat = new SimpleDateFormat("HH:mm");
        shows = new LinkedList<Show>();
        filterShow = new Show(this);
        applyShowFilter();
    }

    public Show getSelectedShow() {
        return selectedShow;
    }

    public void setSelectedShow(Show selectedShow) {
        this.selectedShow = selectedShow;
    }

    private void appendFilter(StringBuilder sb, String parameter, String value, boolean firstParameter) {
        if (firstParameter == false) {
            sb.append("&");
        }
        try {
            sb.append(parameter).append("=").append(URLEncoder.encode(value, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void applyShowFilter() {
        this.shows.clear();
        try {
            StringBuilder requestURL = new StringBuilder(API_ENDPOINT);
            boolean firstParam = true;
            if (filterShow.getArtist().isEmpty() == false) {
                appendFilter(requestURL, "artist", filterShow.getArtist(), firstParam);
                firstParam = false;
            }
            if (filterShow.getTour().isEmpty() == false) {
                appendFilter(requestURL, "tourName", filterShow.getTour(), firstParam);
                firstParam = false;
            }
            if (filterShow.getCountryName().isEmpty() == false) {
                appendFilter(requestURL, "countryName", filterShow.getCountryName(), firstParam);
                firstParam = false;
            }

            System.out.println(requestURL.toString());
            URL url = new URL(requestURL.toString());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() == 200) {
                String xmlString = IOUtils.toString(conn.getInputStream(), "UTF-8");
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder;
                builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new ByteArrayInputStream(
                        xmlString.getBytes("utf-8"))));
                Element root = document.getDocumentElement();
                NodeList showsNodes = root.getElementsByTagName("show");
                for (int temp = 0; temp < showsNodes.getLength(); temp++) {
                    Node showNode = showsNodes.item(temp);
                    if (showNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element showElement = (Element) showNode;
                        String showArtist = showElement.getElementsByTagName("artists").item(0).getTextContent();
                        String showID = showElement.getElementsByTagName("showID").item(0).getTextContent();
                        String showTitle = showElement.getElementsByTagName("title").item(0).getTextContent();
                        String showTour = showElement.getElementsByTagName("tour").item(0).getTextContent();
                        String showVenueName = showElement.getElementsByTagName("venuename").item(0).getTextContent();
                        String showAddress = showElement.getElementsByTagName("address").item(0).getTextContent();
                        String showCity = showElement.getElementsByTagName("city").item(0).getTextContent();
                        String showState = showElement.getElementsByTagName("state").item(0).getTextContent();
                        String showZip = showElement.getElementsByTagName("zip").item(0).getTextContent();
                        String showCountry = showElement.getElementsByTagName("country").item(0).getTextContent();
                        String showCountryName = showElement.getElementsByTagName("countryname").item(0).getTextContent();
                        String showComments = showElement.getElementsByTagName("comments").item(0).getTextContent();
                        String showBuyOnline = showElement.getElementsByTagName("buyonline").item(0).getTextContent();
                        String showTimezone = showElement.getElementsByTagName("timezone").item(0).getTextContent();
                        Show show = new Show(showArtist, showID, showTitle, showTour, null, null, null, showVenueName,
                                showAddress, showCity, showState, showZip, showCountry, showCountryName, -1, -1,
                                showComments,
                                showBuyOnline, showTimezone, this);
                        this.shows.add(show);
                    }
                }
            }
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestContext.getCurrentInstance().update("mainDataTable");
    }

    public Show getFilterShow() {
        return filterShow;
    }

    public void setFilterShow(Show filterShow) {
        this.filterShow = filterShow;
    }

    public LinkedList<Show> getShows() {
        return shows;
    }

    public void setShows(LinkedList<Show> shows) {
        this.shows = shows;
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    public SimpleDateFormat getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(SimpleDateFormat timeFormat) {
        this.timeFormat = timeFormat;
    }

    private SimpleDateFormat dateFormat;
    private SimpleDateFormat timeFormat;

    private boolean artistChkbox = true;
    private boolean showIdChkbox = false;
    private boolean titleChkbox = true;
    private boolean tourChkbox = true;
    private boolean showDateChkbox = false;
    private boolean setTimeChkbox = false;
    private boolean doorTimeChkbox = false;
    private boolean venueNameChkbox = true;
    private boolean addressChkbox = true;
    private boolean cityChkbox = true;
    private boolean stateChkbox = true;
    private boolean zipChkbox = false;
    private boolean countryChkbox = false;
    private boolean countryNameChkbox = true;
    private boolean mapChkbox = false;
    private boolean commentsChkbox = false;
    private boolean buyOnlineChkbox = false;
    private boolean timezoneChkbox = false;

    public boolean isArtistChkbox() {
        return artistChkbox;
    }

    public void setArtistChkbox(boolean artistChkbox) {
        this.artistChkbox = artistChkbox;
    }

    public boolean isShowIdChkbox() {
        return showIdChkbox;
    }

    public void setShowIdChkbox(boolean showIdChkbox) {
        this.showIdChkbox = showIdChkbox;
    }

    public boolean isTitleChkbox() {
        return titleChkbox;
    }

    public void setTitleChkbox(boolean titleChkbox) {
        this.titleChkbox = titleChkbox;
    }

    public boolean isTourChkbox() {
        return tourChkbox;
    }

    public void setTourChkbox(boolean tourChkbox) {
        this.tourChkbox = tourChkbox;
    }

    public boolean isShowDateChkbox() {
        return showDateChkbox;
    }

    public void setShowDateChkbox(boolean showDateChkbox) {
        this.showDateChkbox = showDateChkbox;
    }

    public boolean isSetTimeChkbox() {
        return setTimeChkbox;
    }

    public void setSetTimeChkbox(boolean setTimeChkbox) {
        this.setTimeChkbox = setTimeChkbox;
    }

    public boolean isDoorTimeChkbox() {
        return doorTimeChkbox;
    }

    public void setDoorTimeChkbox(boolean doorTimeChkbox) {
        this.doorTimeChkbox = doorTimeChkbox;
    }

    public boolean isVenueNameChkbox() {
        return venueNameChkbox;
    }

    public void setVenueNameChkbox(boolean venueNameChkbox) {
        this.venueNameChkbox = venueNameChkbox;
    }

    public boolean isAddressChkbox() {
        return addressChkbox;
    }

    public void setAddressChkbox(boolean addressChkbox) {
        this.addressChkbox = addressChkbox;
    }

    public boolean isCityChkbox() {
        return cityChkbox;
    }

    public void setCityChkbox(boolean cityChkbox) {
        this.cityChkbox = cityChkbox;
    }

    public boolean isStateChkbox() {
        return stateChkbox;
    }

    public void setStateChkbox(boolean stateChkbox) {
        this.stateChkbox = stateChkbox;
    }

    public boolean isZipChkbox() {
        return zipChkbox;
    }

    public void setZipChkbox(boolean zipChkbox) {
        this.zipChkbox = zipChkbox;
    }

    public boolean isCountryChkbox() {
        return countryChkbox;
    }

    public void setCountryChkbox(boolean countryChkbox) {
        this.countryChkbox = countryChkbox;
    }

    public boolean isCountryNameChkbox() {
        return countryNameChkbox;
    }

    public void setCountryNameChkbox(boolean countryNameChkbox) {
        this.countryNameChkbox = countryNameChkbox;
    }

    public boolean isMapChkbox() {
        return mapChkbox;
    }

    public void setMapChkbox(boolean mapChkbox) {
        this.mapChkbox = mapChkbox;
    }

    public boolean isCommentsChkbox() {
        return commentsChkbox;
    }

    public void setCommentsChkbox(boolean commentsChkbox) {
        this.commentsChkbox = commentsChkbox;
    }

    public boolean isBuyOnlineChkbox() {
        return buyOnlineChkbox;
    }

    public void setBuyOnlineChkbox(boolean buyOnlineChkbox) {
        this.buyOnlineChkbox = buyOnlineChkbox;
    }

    public boolean isTimezoneChkbox() {
        return timezoneChkbox;
    }

    public void setTimezoneChkbox(boolean timezoneChkbox) {
        this.timezoneChkbox = timezoneChkbox;
    }

    public String detail(Show show) {
        System.out.println("################" + show.getArtist());
        return "/index.xhtml?faces-redirect=true";
    }

}
